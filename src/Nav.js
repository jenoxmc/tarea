import React from 'react';
import "./index.css";
import Body from "./Body";

export default class Nav extends React.Component{
    
    constructor(){
		super();
		this.setState({
			url: ""
		})
	}

	handleChange(event){
		let valorInput = event.target.value;
		this.setState({ url: valorInput});
		console.log(this.state);
    }
    
    alertar(e){
        e.preventDefault();
        var nuevoobjeto = {
            src: this.state.url,
            top: false,
            id: lsu //no consigo que me tire el id
        }
		var localstorage = JSON.parse(localStorage.getItem("datos"));
        var lsu = localstorage.push(nuevoobjeto);
        console.log(lsu) //Me suelta el valor de la ubicación del nuevo objeto [26], etc
        localStorage.setItem('datos', JSON.stringify(localstorage));
	}

    //https://pbs.twimg.com/profile_images/1117992410888515585/VLinf1AQ_400x400.jpg
    
    render(){
        return(
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <a className="navbar-brand" href="#!">Navbar</a>
                <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul className="navbar-nav mr-auto mt-2 mt-md-0 ">
                    <li className="nav-item active">
                        <a className="nav-link" href="#!">Home <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#!">Link</a>
                    </li>
                    </ul>
                </div>
                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-upload"></i></button>
                <div className="dropdown-menu dropdown-menu-right ml-2 mr-4 pl-2 pr-2" aria-labelledby="dropdownMenu1">
                    <form onSubmit={(e) => this.alertar(e)}>
                        <input className="dropdown-item p-2 w-100" placeholder="Nombre" href="#!"></input>
                        <input className="dropdown-item p-2 w-100" onChange={(evento) => this.handleChange(evento)} placeholder="url" href="#!"></input>
                        <button className="btn btn-primary mt-1" type="submit">Enviar</button>
                    </form>
                </div>
            </nav>
        )
    }
}