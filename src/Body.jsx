import React from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import "./index.css";

export default class Body extends React.Component{
    
    constructor(props){
        super(props);
        this.state={
            meme: [],
        }
    }	

    componentDidMount(){
        
        //Colección de 25 objetos
        /*var datos = [{
        src: "https://images.mediotiempo.com/7etcbgMMuunxEMr7GnCKMmk9sZw=/0x530/uploads/media/2019/11/10/memes-liga-mx-mejores-jornada-207.jpg",
        id: 1,
        top: true    
    },{
        src: "https://www.eluniversal.com.mx/sites/default/files/styles/f03-651x400/public/2019/11/12/el_buen_fin_2019_memes_.jpg?itok=qQ_0v3hA",
        id: 2,
        top: true    
    },{
        src: "https://www.elsoldepuebla.com.mx/incoming/j1mmw-meme.jpg/alternates/LANDSCAPE_400/meme.jpg",
        id: 3,
        top: true    
    },{
        src: "http://s3-us-west-1.amazonaws.com/prod-glamorama/wp-content/uploads/2019/11/13171900/edmundo-memes-p2.jpg",
        id: 4,
        top: true    
    },{
        src: "https://e00-marca.uecdn.es/assets/multimedia/imagenes/2019/04/28/15564493537391.jpg",
        id: 5,
        top: true    
    },{
        src: "https://ep01.epimg.net/verne/imagenes/2017/11/09/articulo/1510239247_329890_1510242733_noticia_normal.jpg",
        id: 6,
        top: true    
    },{
        src: "https://img.depor.com/files/ec_article_multimedia_gallery/uploads/2019/07/08/5d235b51acd5e.jpeg",
        id: 7,
        top: true    
    },{
        src: "https://infocielo.com/uploads/noticias/imagenes/a/2019/04/20190424085954_mejores-memes-655x368.jpg",
        id: 8,
        top: true    
    },{
        src: "https://s3.eestatic.com/2019/10/19/elbernabeu/futbol/Futbol-La_Liga-Primera_Division-RCD_Mallorca-Real_Madrid-Memes-Futbol_437969678_135687671_1706x960.jpg",
        id: 9,
        top: true    
    },{
        src: "https://cdn-3.expansion.mx/dims4/default/deada32/2147483647/strip/true/crop/670x500+0+0/resize/800x597!/quality/90/?url=https%3A%2F%2Fcdn-3.expansion.mx%2Fa1%2Fab%2Ff611be5c4bba99797b6bb6d81846%2Fwhatsapp-image-2018-10-26-at-1.47.57%20PM.jpeg",
        id: 10,
        top: true
    },{
        src: "https://images.thestar.com/1xrv-qmmBKZYw_1wTCPaQpi9FpU=/0x0:584x344/1086x640/smart/filters:cb(1570461387913)/https://www.thestar.com/content/dam/thestar/vancouver/2019/10/05/why-political-memes-which-are-virtually-unregulated-matter-to-this-federal-election/_1_harper_meme.jpg",
        id: 11,
        top: false
    },{
        src: "https://chile.as.com/chile/imagenes/2019/08/25/album/1566768455_702773_1566768581_album_grande.jpg",
        id: 12,
        top: false
    },{
        src: "https://t.resfu.com/media/img_news/meme-10-del-barca-levante-2019-20--memedeportes.jpg",
        id: 13,
        top: false
    },{
        src: "https://www.fundeu.es/wp-content/uploads/2013/02/RecMemes.jpg",
        id: 14,
        top: false
    },{
        src: "https://www.fosi.org/media/images/funny-game-of-thrones-memes-coverimage.width-800.jpg",
        id: 15,
        top: false
    },{
        src: "https://www.elsoldemexico.com.mx/doble-via/virales/kd9e6w-memes-buen-fin.jpg/ALTERNATES/LANDSCAPE_400/memes-buen-fin.jpg",
        id: 16,
        top: false
    },{
        src: "https://elcomercio-depor-prod.cdn.arcpublishing.com/resizer/xcaxghYn5gVHTfJoXjqy0SCTZx4=/980x528/smart/arc-anglerfish-arc2-prod-elcomercio.s3.amazonaws.com/public/HA5E64MBDRBVDFGETHVPEIHEC4.jpg",
        id: 17,
        top: false
    },{
        src: "https://d2slcw3kip6qmk.cloudfront.net/marketing/blogs/press/dont-ask/how_to_use_memes_for_marketing.jpg",
        id: 18,
        top: false
    },{
        src: "https://lospleyers.com/wp-content/uploads/2019/09/Tigres-Monterrey-Cla%CC%81sico-Regio-Memes-e1569724908423.jpeg",
        id: 19,
        top: false
    },{
        src: "https://cdn.heraldodemexico.com.mx/wp-content/uploads/2019/11/12174754/Evo_Morales_claudio_Suarez.jpg",
        id: 20,
        top: false
    },{
        src: "https://assets3.thrillist.com/v1/image/2830143/size/gn-gift_guide_variable_c.jpg",
        id: 21,
        top: false
    },{
        src: "https://www.infotechnology.com/__export/1565699085911/sites/revistait/img/2019/08/13/13_08_19_dolar_portada.jpg_1917179645.jpg",
        id: 22,
        top: false
    },{
        src: "https://www.theclinic.cl/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-30-at-14.05.56.jpeg",
        id: 23,
        top: false
    },{
        src: "https://www.washingtonpost.com/resizer/QZnZZF9FhcQVBHhqBzBEUqCMJGs=/767x0/smart/arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/3ORNMTD4CE3EBAGAWDAHECMM4E.jpg",
        id: 24,
        top: false
    },{
        src: "https://imagenes.heraldo.es/files/image_990_v3/uploads/imagenes/2019/09/04/memes-para-alumnos-profesores-y-padres-para-sobrevivir-a-la-vuelta-al-cole.jpeg",
        id: 25,
        top: false
        }]

        localStorage.setItem('datos', JSON.stringify(datos));*/

        var localstorage = JSON.parse(localStorage.getItem("datos"));
        var lsf = localstorage.filter(item => item.top === true);

        console.log(localstorage);

        this.setState({
            meme: lsf
        })
        
    }

    render(){
        return(
            <div className="container-fluid mt-2">
                <h1>TOP Memes</h1>
                    <div className="row w-100 card-columns">        
                        {this.state.meme.map((item) => (
                            <div className="">
                                <div className="d-flex card">
                                    <Link to={"/photo/" + item.id}>
                                        <img alt="" className="card-img-top a" src={item.src}></img>
                                    </Link>
                                </div>
                            </div>
                        ))}    
                    </div>
            </div>
        )
    }
}