import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

export default function Photo() {
    const parametro = useParams().id;
    const [meme, setMeme] = useState({ 
        meme: {}
    });

    useEffect(() => {
        const updateStorage = () => {    
            var localstorage = JSON.parse(localStorage.getItem("datos"));
            var lsf = localstorage.find(item => item.id==parametro) 
            console.log(lsf)
            setMeme(lsf)
            /*fetch('http://jsonplaceholder.typicode.com/photos/' + parametro)
                .then(resp => resp.json())
                .then(data => setMeme(data))*/
        }

        updateStorage()
    }, [])

    return(
        <div className="d-flex justify-content-center align-item-center">
            <div>
                <h1 className="d-flex justify-content-center">ESTA ES TU IMAGEN!</h1>
                <img alt="" src={meme.src}></img>
            </div>
        </div>
    )
}
