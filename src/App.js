import React from 'react';
import Nav from "./Nav";
import Body from "./Body";
import Body2 from './Body2';
import Photo from "./Photo";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";


export default class App extends React.Component{
  render(){
    return(
      <Router>
        <Switch>
          <Route exact path="/">
            <Nav/>
            <Body/>
            <Body2/>
          </Route>
          <Route path="/photo/:id">
            <Photo/>
          </Route>
        </Switch>
		  </Router>
    )
  }
}
